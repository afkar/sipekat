<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['konten']='v_admin_petugas';
        $data['judul']='Home';
        $this->load->view('v_admin_pengaju', $data);
		// $this->load->view('v_home_pengaju');
	}

	public function home_pengaju()
	{
		$data['konten']='v_home_pengaju';
        $data['judul']='Home';
        $this->load->view('v_admin_pengaju', $data);
	}

	public function register()
	{
		$this->load->view('v_register.php');
	}

	public function forgot_password()
	{
		$this->load->view('v_forgot_password.php');
	}

	public function login()
	{
		$this->load->view('v_login.php');
	}

	public function surat_pengajuan_ktp()
	{
		$data['konten']='v_surat_pengajuan_ktp';
        $data['judul']='Surat Pengajuan KTP';
        $this->load->view('v_admin_pengaju', $data);
	}

	public function add_surat()
	{
		$data['konten']='v_add_surat';
        $data['judul']='Formulir Pengajuan KTP';
        $this->load->view('v_admin_pengaju', $data);
	}

	public function profile()
	{
		$data['konten']='v_profile';
        $data['judul']='Profile';
        $this->load->view('v_admin_pengaju', $data);
	}

	public function home_petugas()
	{
		$data['konten']='v_admin_petugas';
        $data['judul']='Home';
        $this->load->view('v_admin_pengaju', $data);
	}

	public function pengajuan_surat()
	{
		$data['konten']='v_pengajuan_surat';
        $data['judul']='Home';
        $this->load->view('v_admin_pengaju', $data);
	}

	public function detail_surat()
	{
		$data['konten']='v_detail_surat';
        $data['judul']='Home';
        $this->load->view('v_admin_pengaju', $data);
	}

	public function test()
	{
		$data['konten']='v_test';
        $data['judul']='Home';
        $this->load->view('v_admin_pengaju', $data);
	}
}
