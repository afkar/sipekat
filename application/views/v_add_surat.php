<section class="content">
        <div class="container-fluid">
            <ol class="breadcrumb breadcrumb-bg-cyan">
                <li><a href="javascript:void(0);"><i class="material-icons">home</i> <?php echo $judul; ?></a></li>
            </ol>
            
            <!-- Advanced Form Example With Validation -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card no-radius">
                    <div class="header row">
                            <div class="col-sm-6">
                                <h2>PENGAJUAN KTP</h2>  
                            </div>
                            <div class="col-sm-6 align-right js-sweetalert">
                                <button type="button" class="btn bg-amber waves-effect" data-toggle="modal" data-target="#defaultModal">
                                    <i class="material-icons">info_outline</i>
                                    <span>INFO PERSYARATAN</span>
                                </button>
                            </div>                            
                        </div>
                        <div class="body">
                            <form id="wizard_with_validation" method="POST">
                                <h3>Data Pemohon</h3>
                                <fieldset>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="nik" value="3531527575" disabled>
                                            <label class="form-label">NIK</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="nama" id="nama" value="Agus" disabled>
                                            <label class="form-label">Nama Lengkap</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="jenis_kelamin" id="jenis_kelamin" value="Laki-laki" disabled>
                                            <label class="form-label">Jenis Kelamin</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" value="Kota Malang" disabled>
                                            <label class="form-label">Tempat Lahir</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="tanggal_lahir" id="tanggal_lahir" value="15-12-1997" disabled>
                                            <label class="form-label">Tanggal Lahir</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="alamat" id="alamat" value="Jl Anggrek Garuda" disabled>
                                            <label class="form-label">Alamat</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="status_perkawinan" id="status_perkawinan" value="Belum Menikah" disabled>
                                            <label class="form-label">Status Perkawinan</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="pekerjaan" id="pekerjaan" value="Karyawan Swasta" disabled>
                                            <label class="form-label">Pekerjaan</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="email" id="email" value="agus@gmail.com" disabled>
                                            <label class="form-label">Email</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="no_telepon" id="no_telepon" value="0856746761" disabled>
                                            <label class="form-label">No Telepon</label>
                                        </div>
                                    </div>
                                </fieldset>

                                <h3>Data Pendukung</h3>
                                <fieldset>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="name" class="form-control" name="no_kk" id="no_kk" required>
                                            <label class="form-label">Nomor Kartu Keluarfga*</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="nama_pendukung" class="form-control" required>
                                            <label class="form-label">Nama Lengkap*</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="nik_pendukung" class="form-control" required>
                                            <label class="form-label">NIK*</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="email" name="email" class="form-control" required>
                                            <label class="form-label">Email*</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <textarea name="address" cols="30" rows="3" class="form-control no-resize" required></textarea>
                                            <label class="form-label">Address*</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input min="18" type="number" name="age" class="form-control" required>
                                            <label class="form-label">Age*</label>
                                        </div>
                                        <div class="help-info">The warning step will show up if age is less than 18</div>
                                    </div>
                                </fieldset>

                                <h3>Keterangan</h3>
                                <fieldset>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" name="name" class="form-control" required>
                                            <label class="form-label">First Name*</label>
                                        </div>
                                    </div>
                                </fieldset>

                                <h3>Berkas Kelengkapan</h3>
                                <fieldset>
                                    <input id="acceptTerms-2" name="acceptTerms" type="checkbox" required>
                                    <label for="acceptTerms-2">I agree with the Terms and Conditions.</label>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Advanced Form Example With Validation -->
        </div>
        <!-- Default Size -->
        <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">INFO PERSYARATAN</h4>
                    </div>
                    <div class="modal-body">
                    <ul class="list-group">
                        <li class="list-group-item">1. Kartu Keluarga</li>
                        <li class="list-group-item">2. KTP Lama (untuk KTP Rusak dan atau Perubahan Elemen Data)</li>
                        <li class="list-group-item">3. Surat Kehilangan Kepolisian (untuk KTP Hilang)</li>
                        <li class="list-group-item">4. Dokumen Pendukung Lainnya (untuk Perubahan Elemen Data)</li>
                        <li class="list-group-item">5. Dokumen Perjalanan (WNA)</li>
                        <li class="list-group-item">6. KITAP (WNA)</li>
                        <li class="list-group-item">7. Lain-lain 1</li>
                        <li class="list-group-item">8. Lain-lain 2</li>
                        <li class="list-group-item">9. Lain-lain 3</li>
                    </ul>
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-link waves-effect">SAVE CHANGES</button> -->
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
</section>