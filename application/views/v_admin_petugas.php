<section class="content">
    <div class="container-fluid">
        <ol class="breadcrumb breadcrumb-bg-cyan">
            <li><a href="javascript:void(0);"><i class="material-icons">home</i> <?php echo $judul; ?></a></li>
        </ol>
        <!-- Exportable Table -->
        <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card no-radius">
                        <div class="header row">
                            <div class="col-sm-6">
                                <h2>
                                    List Pengajuan Akun
                                </h2>
                            </div>
                            <div class="col-sm-6 align-right">
                                <button type="button" class="btn btn-primary waves-effect" onclick="location.href='<?=base_url()?>register'">
                                    <i class="material-icons">create</i>
                                    <span>Buat Akun Baru</span>
                                </button>
                            </div>                            
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Detail</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Tiger Nixon</td>
                                            <td>System Architect</td>
                                            <td class="align-center">
                                                <button type="button" class="btn btn-success waves-effect" onclick="location.href='<?=base_url()?>register?type=konfirmasi'">
                                                    <i class="material-icons">info_outline</i>
                                                    <span>Details</span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Garrett Winters</td>
                                            <td>Accountant</td>
                                            <td class="align-center">
                                                <button type="button" class="btn btn-success waves-effect" onclick="location.href='<?=base_url()?>register'">
                                                    <i class="material-icons">info_outline</i>
                                                    <span>Details</span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Ashton Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td class="align-center">
                                                <button type="button" class="btn btn-success waves-effect" onclick="location.href='<?=base_url()?>register'">
                                                    <i class="material-icons">info_outline</i>
                                                    <span>Details</span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Cedric Kelly</td>
                                            <td>Senior Javascript Developer</td>
                                            <td class="align-center">
                                                <button type="button" class="btn btn-success waves-effect" onclick="location.href='<?=base_url()?>register'">
                                                    <i class="material-icons">info_outline</i>
                                                    <span>Details</span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Airi Satou</td>
                                            <td>Accountant</td>
                                            <td class="align-center">
                                                <button type="button" class="btn btn-success waves-effect" onclick="location.href='<?=base_url()?>register'">
                                                    <i class="material-icons">info_outline</i>
                                                    <span>Details</span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Brielle Williamson</td>
                                            <td>Integration Specialist</td>
                                            <td class="align-center">
                                                <button type="button" class="btn btn-success waves-effect" onclick="location.href='<?=base_url()?>register'">
                                                    <i class="material-icons">info_outline</i>
                                                    <span>Details</span>
                                                </button>
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
    </div>
</section>