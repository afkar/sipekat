<section class="content">
        <div class="container-fluid">
            <ol class="breadcrumb breadcrumb-bg-cyan">
                <li><a href="javascript:void(0);"><i class="material-icons">home</i> <?php echo $judul; ?></a></li>
            </ol>
            <div class="card">
                <div class="body">
                    <form id="sign_up" method="POST">
                        <!-- <div class="msg">Daftar pemohon</div> -->
                        <div class="row">
                            <ol class="breadcrumb breadcrumb-bg-cyan">
                                <li>Data Pemohon</li>
                            </ol>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-6">                            
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">person</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="nik" placeholder="NIK" required autofocus>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">person</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="nomor_kk" placeholder="Nomor KK" required autofocus>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-6">                            
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">person</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="nama" placeholder="Nama" required autofocus>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">email</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="email" placeholder="Email" required autofocus>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <ol class="breadcrumb breadcrumb-bg-cyan">
                                <li>Data Pendukung</li>
                            </ol>
                        </div>

                        <div class="row clearfix">
                            <div class="col-md-6">                            
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">lock</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="password" class="form-control" name="password" minlength="8" placeholder="Password" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">lock</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="password" class="form-control" name="confirm" minlength="8" placeholder="Konfirmasi Password" required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <ol class="breadcrumb breadcrumb-bg-cyan">
                                <li>Keterangan</li>
                            </ol>
                        </div>

                        <div class="row clearfix">
                            <div class="col-md-6">                            
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">lock</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="password" class="form-control" name="password" minlength="8" placeholder="Password" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">lock</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="password" class="form-control" name="confirm" minlength="8" placeholder="Konfirmasi Password" required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <ol class="breadcrumb breadcrumb-bg-cyan">
                                <li>Berkas Kelengkapan</li>
                            </ol>
                        </div>

                        <div class="row clearfix">
                            <div class="col-md-6">                            
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">lock</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="password" class="form-control" name="password" minlength="8" placeholder="Password" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">lock</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="password" class="form-control" name="confirm" minlength="8" placeholder="Konfirmasi Password" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <!-- <div class="form-group">
                            <input type="checkbox" name="terms" id="terms" class="filled-in chk-col-pink">
                            <label for="terms">I read and agree to the <a href="javascript:void(0);">terms of usage</a>.</label>
                        </div> -->
                        <div class="row js-sweetalert">
                            <div class="col-sm-6">
                                <button type="button" class="btn btn-block btn-success waves-effect" data-type="success">
                                    <i class="material-icons">check</i>
                                    <span>Terima</span>
                                </button>
                            </div>
                            <div class="col-sm-6">
                                <button type="button" class="btn btn-block bg-red waves-effect" data-type="confirm">
                                    <i class="material-icons">cancel</i>
                                    <span>Tolak</span>
                                </button>
                            </div>
                        </div>
                        
                        <!-- <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">TERIMA</button> -->
                    </form>
                </div>
            </div>
        </div>
</section>
