<?php
$type = isset($_GET['type']) ? $_GET['type'] : "register";
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Daftar | Sipekat</title>
    <!-- Favicon-->
    <link rel="icon" href="assets/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?=base_url()?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">

    <!-- Waves Effect Css -->
    <link href="<?=base_url()?>assets/plugins/node-waves/waves.css" rel="stylesheet" type="text/css">

    <!-- Animation Css -->
    <link href="<?=base_url()?>assets/plugins/animate-css/animate.css" rel="stylesheet" type="text/css">

    <!-- Sweetalert Css -->
    <link href="<?=base_url()?>assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />

    <!-- Custom Css -->
    <link href="<?=base_url()?>assets/css/style.css" rel="stylesheet" type="text/css">
</head>

<body class="signup-page">
    <div class="signup-box">
        <div class="logo">
        <?php 
            if($type == "register"){
        ?>
            <a href="javascript:void(0);">Daftar Sipekat</a>
            <small>Sistem Pelayanan Masyarakat Terpadu</small>
        <?php } else { ?>
            <a href="javascript:void(0);">Data Pengaju</a>
        <?php } ?>
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_up" method="POST">
                    <!-- <div class="msg">Daftar pemohon</div> -->
                    <div class="row">
                        <ol class="breadcrumb breadcrumb-bg-cyan">
                            <li>Data Pribadi</li>
                        </ol>
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-6">                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" name="nik" placeholder="NIK" required autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" name="nomor_kk" placeholder="Nomor KK" required autofocus>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-6">                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" name="nama" placeholder="Nama" required autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">email</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" name="email" placeholder="Email" required autofocus>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-6">                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">place</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" name="tempat_lahir" placeholder="Tempat Lahir" required autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">date_range</i>
                                </span>
                                <div class="form-line" id="bs_datepicker_container">
                                    <input type="text" class="form-control" placeholder="Tanggal Lahir" name="tanggal_lahir">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-6">                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">phone_iphone</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" name="no_telepon" placeholder="No Telepon" required autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">work</i>
                                </span>
                                <div class="form-line">
                                    <select class="form-control show-tick">
                                        <option value="">Pilih Pekerjaan</option>
                                        <option value="BELUM/TIDAK BEKERJA">BELUM/TIDAK BEKERJA</option>
                                        <option value="BELUM/TIDAK BEKERJA">BELUM/TIDAK BEKERJA</option>
                                        <option value="MENGURUS RUMAH TANGGA">MENGURUS RUMAH TANGGA</option>
                                        <option value="PELAJAR/MAHASISWA">PELAJAR/MAHASISWA</option>
                                        <option value="PENSIUNAN">PENSIUNAN</option>
                                        <option value="AKUNTAN">AKUNTAN</option>
                                        <option value="ANGGOTA BPK">ANGGOTA BPK</option>
                                        <option value="ANGGOTA DPD">ANGGOTA DPD</option>
                                        <option value="ANGGOTA DPR-RI">ANGGOTA DPR-RI</option>
                                        <option value="ANGGOTA DPRD KABUPATEN/KOTA">ANGGOTA DPRD KABUPATEN/KOTA</option>
                                        <option value="ANGGOTA DPRD PROVINSI">ANGGOTA DPRD PROVINSI</option>
                                        <option value="ANGGOTA KABINET/KEMENTERIAN">ANGGOTA KABINET/KEMENTERIAN</option>
                                        <option value="ANGGOTA MAHKAMAH KONSTITUSI">ANGGOTA MAHKAMAH KONSTITUSI</option>
                                        <option value="APOTEKER">APOTEKER</option>
                                        <option value="ARSITEK">ARSITEK</option>
                                        <option value="BIARAWATI">BIARAWATI</option>
                                        <option value="BIDAN">BIDAN</option>
                                        <option value="BUPATI">BUPATI</option>
                                        <option value="BURUH HARIAN LEPAS">BURUH HARIAN LEPAS</option>
                                        <option value="BURUH NELAYAN/PERIKANAN">BURUH NELAYAN/PERIKANAN</option>
                                        <option value="BURUH PETERNAKAN">BURUH PETERNAKAN</option>
                                        <option value="BURUH TANI/PERKEBUNAN">BURUH TANI/PERKEBUNAN</option>
                                        <option value="DOKTER">DOKTER</option>
                                        <option value="DOSEN">DOSEN</option>
                                        <option value="DUTA BESAR">DUTA BESAR</option>
                                        <option value="GUBERNUR">GUBERNUR</option>
                                        <option value="GURU">GURU</option>
                                        <option value="IMAM MESJID">IMAM MESJID</option>
                                        <option value="INDUSTRI">INDUSTRI</option>
                                        <option value="JURU MASAK">JURU MASAK</option>
                                        <option value="KARYAWAN BUMD">KARYAWAN BUMD</option>
                                        <option value="KARYAWAN BUMN">KARYAWAN BUMN</option>
                                        <option value="KARYAWAN HONORER">KARYAWAN HONORER</option>
                                        <option value="KARYAWAN SWASTA">KARYAWAN SWASTA</option>
                                        <option value="KEPALA DESA">KEPALA DESA</option>
                                        <option value="KEPOLISIAN RI">KEPOLISIAN RI</option>
                                        <option value="KONSTRUKSI">KONSTRUKSI</option>
                                        <option value="KONSULTAN">KONSULTAN</option>
                                        <option value="MEKANIK">MEKANIK</option>
                                        <option value="NELAYAN/PERIKANAN">NELAYAN/PERIKANAN</option>
                                        <option value="NOTARIS">NOTARIS</option>
                                        <option value="PARAJI">PARAJI</option>
                                        <option value="PARANORMAL">PARANORMAL</option>
                                        <option value="PASTOR">PASTOR</option>
                                        <option value="PEDAGANG">PEDAGANG</option>
                                        <option value="PEGAWAI NEGERI SIPIL">PEGAWAI NEGERI SIPIL</option>
                                        <option value="PELAUT">PELAUT</option>
                                        <option value="PEMBANTU RUMAH TANGGA">PEMBANTU RUMAH TANGGA</option>
                                        <option value="PENATA BUSANA">PENATA BUSANA</option>
                                        <option value="PENATA RAMBUT">PENATA RAMBUT</option>
                                        <option value="PENATA RIAS">PENATA RIAS</option>
                                        <option value="PENDETA">PENDETA</option>
                                        <option value="PENELITI">PENELITI</option>
                                        <option value="PENGACARA">PENGACARA</option>
                                        <option value="PENTERJEMAH">PENTERJEMAH</option>
                                        <option value="PENYIAR RADIO">PENYIAR RADIO</option>
                                        <option value="PENYIAR TELEVISI">PENYIAR TELEVISI</option>
                                        <option value="PERANCANG BUSANA">PERANCANG BUSANA</option>
                                        <option value="PERANGKAT DESA">PERANGKAT DESA</option>
                                        <option value="PERAWAT">PERAWAT</option>
                                        <option value="PERDAGANGAN">PERDAGANGAN</option>
                                        <option value="PETANI/PEKEBUN">PETANI/PEKEBUN</option>
                                        <option value="PETERNAK">PETERNAK</option>
                                        <option value="PIALANG">PIALANG</option>
                                        <option value="PILOT">PILOT</option>
                                        <option value="PRESIDEN">PRESIDEN</option>
                                        <option value="PROMOTOR ACARA">PROMOTOR ACARA</option>
                                        <option value="PSIKIATER/PSIKOLOG">PSIKIATER/PSIKOLOG</option>
                                        <option value="SENIMAN">SENIMAN</option>
                                        <option value="SOPIR">SOPIR</option>
                                        <option value="TABIB">TABIB</option>
                                        <option value="TENTARA NASIONAL INDONESIA">TENTARA NASIONAL INDONESIA</option>
                                        <option value="TRANSPORTASI">TRANSPORTASI</option>
                                        <option value="TUKANG BATU">TUKANG BATU</option>
                                        <option value="TUKANG CUKUR">TUKANG CUKUR</option>
                                        <option value="TUKANG GIGI">TUKANG GIGI</option>
                                        <option value="TUKANG JAHIT">TUKANG JAHIT</option>
                                        <option value="TUKANG KAYU">TUKANG KAYU</option>
                                        <option value="TUKANG LAS/PANDAI BESI">TUKANG LAS/PANDAI BESI</option>
                                        <option value="TUKANG LISTRIK">TUKANG LISTRIK</option>
                                        <option value="TUKANG SOL SEPATU">TUKANG SOL SEPATU</option>
                                        <option value="USTADZ/MUBALIGH">USTADZ/MUBALIGH</option>
                                        <option value="WAKIL BUPATI">WAKIL BUPATI</option>
                                        <option value="WAKIL GUBERNUR">WAKIL GUBERNUR</option>
                                        <option value="WAKIL PRESIDEN">WAKIL PRESIDEN</option>
                                        <option value="WAKIL WALIKOTA">WAKIL WALIKOTA</option>
                                        <option value="WALIKOTA">WALIKOTA</option>
                                        <option value="WARTAWAN">WARTAWAN</option>
                                        <option value="WIRASWASTA">WIRASWASTA</option>
                                        <option value="LAINNYA">LAINNYA</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-6">                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">place</i>
                                </span>
                                <div class="form-line">
                                    <textarea name="alamat" cols="30" rows="3" class="form-control no-resize" required placeholder="Alamat tempat tinggal"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-line" id="bs_datepicker_container">
                                    <select class="form-control show-tick">
                                        <option value="">Pilih Jenis Kelamin</option>
                                        <option value="laki-laki">Laki-laki</option>
                                        <option value="perempuan">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-6">                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-line" id="bs_datepicker_container">
                                    <select class="form-control show-tick">
                                        <option value="islam">Islam</option>
                                        <option value="kristen">Kristen</option>
                                        <option value="katholik ">Katholik</option>
                                        <option value="hindu">Hindu</option>
                                        <option value="budha">Budha</option>
                                        <option value="konghucu">Konghucu</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-line" id="bs_datepicker_container">
                                    <select class="form-control show-tick">
                                        <option value="">Pilih Pendidikan Terakhir</option>
                                        <option value="Tidak/Belum Sekolah">Tidak/Belum Sekolah</option>
                                        <option value="Belum Tamat SD">Belum Tamat SD</option>
                                        <option value="SD/Sederajat">SD/Sederajat</option>
                                        <option value="SLTP/Sederajat">SLTP/Sederajat</option>
                                        <option value="SLTA/Sederajat">SLTA/Sederajat</option>
                                        <option value="Diploma I">Diploma I</option>
                                        <option value="Diploma II">Diploma II</option>
                                        <option value="Akademi/Diploma III/S. Muda">Akademi/Diploma III/S. Muda</option>
                                        <option value="Diploma IV/Strata I">Diploma IV/Strata I</option>
                                        <option value="Strata II">Strata II</option>
                                        <option value="Strata III">Strata III</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <ol class="breadcrumb breadcrumb-bg-cyan">
                            <li>Password yang diinginkan (Minimal 8 Karakter)</li>
                        </ol>
                    </div>

                    <div class="row clearfix">
                        <div class="col-md-6">                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock</i>
                                </span>
                                <div class="form-line">
                                    <input type="password" class="form-control" name="password" minlength="8" placeholder="Password" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock</i>
                                </span>
                                <div class="form-line">
                                    <input type="password" class="form-control" name="confirm" minlength="8" placeholder="Konfirmasi Password" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <ol class="breadcrumb breadcrumb-bg-cyan">
                            <li>Lampiran</li>
                        </ol>
                    </div>

                    <div class="card" style="border-radius:0px;">
                        <div class="header">
                            <h2>
                                UPLOAD KTP
                            </h2>                            
                        </div>
                        <div class="body">
                            <!-- <form action="/" id="frmFileUploadKTP" class="dropzone" method="post" enctype="multipart/form-data"> -->
                                <!-- <div class="dz-message">                                    
                                    <h3>Drop files here or click to upload.</h3>                    
                                </div> -->
                                <div class="fallback">
                                    <input name="fileKTP" type="file" multiple />
                                </div>
                            <!-- </form> -->
                        </div>
                    </div>

                    <div class="card" style="border-radius:0px;">
                        <div class="header">
                            <h2>
                                UPLOAD KK
                            </h2>                            
                        </div>
                        <div class="body">
                            <!-- <form action="/" id="frmFileUploadKK" class="dropzone" method="post" enctype="multipart/form-data"> -->
                                <!-- <div class="dz-message">                                    
                                    <h3>Drop files here or click to upload.</h3>                    
                                </div> -->
                                <div class="fallback">
                                    <input name="fileKK" type="file" multiple />
                                </div>
                            <!-- </form> -->
                        </div>
                    </div>
                    
                    
                    <!-- <div class="form-group">
                        <input type="checkbox" name="terms" id="terms" class="filled-in chk-col-pink">
                        <label for="terms">I read and agree to the <a href="javascript:void(0);">terms of usage</a>.</label>
                    </div> -->
                    <?php 
                    if($type == "register"){
                    ?>
                    <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">DAFTAR</button>

                    <div class="m-t-25 m-b--5 align-center">
                        <a href="<?=base_url()?>login">Sudah memiliki akun?</a>
                    </div>
                    <?php }
                    else {
                    ?>
                    <div class="row js-sweetalert">
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-block btn-success waves-effect" data-type="success">
                                <i class="material-icons">check</i>
                                <span>Terima</span>
                            </button>
                        </div>
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-block bg-red waves-effect" data-type="confirm">
                                <i class="material-icons">cancel</i>
                                <span>Tolak</span>
                            </button>
                        </div>
                    </div>
                    
                    <!-- <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">TERIMA</button> -->
                    <?php } ?>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="<?=base_url()?>assets/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?=base_url()?>assets/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?=base_url()?>assets/plugins/node-waves/waves.js"></script>

    <!-- Autosize Plugin Js -->
    <script src="<?=base_url()?>assets/plugins/autosize/autosize.js"></script>

    <!-- Moment Plugin Js -->
    <script src="<?=base_url()?>assets/plugins/momentjs/moment.js"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="<?=base_url()?>assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

    <!-- Bootstrap Datepicker Plugin Js -->
    <script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?=base_url()?>assets/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="<?=base_url()?>assets/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Dropzone Plugin Js -->
    <script src="<?=base_url()?>assets/plugins/dropzone/dropzone.js"></script>

    <!-- Custom Js -->
    <script src="<?=base_url()?>assets/js/admin.js"></script>
    <script src="<?=base_url()?>assets/js/pages/examples/sign-in.js"></script>
    <script src="<?=base_url()?>assets/js/pages/forms/basic-form-elements.js"></script>
    <script src="<?=base_url()?>assets/js/pages/ui/dialogs.js"></script>

    <script>

    </script>
</body>

</html>