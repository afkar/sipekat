<section class="content">
        <div class="container-fluid">
            <ol class="breadcrumb breadcrumb-bg-cyan">
                <li><a href="javascript:void(0);"><i class="material-icons">home</i> <?php echo $judul; ?></a></li>
            </ol>
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card no-radius">
                        <div class="header row">
                            <div class="col-sm-6">
                                <h2>
                                    List Permohonan KTP
                                </h2>
                            </div>
                            <div class="col-sm-6 align-right">
                                <button type="button" class="btn btn-primary waves-effect" onclick="location.href='<?=base_url()?>add_surat'">
                                    <i class="material-icons">create</i>
                                    <span>Buat Baru</span>
                                </button>
                            </div>                            
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Id Transaksi</th>
                                            <th>No Surat</th>
                                            <th>Nama Pemohon</th>
                                            <th>Alamat Pemohon</th>
                                            <th>Status Surat</th>
                                            <th>Tanggal Surat Dibuat</th>
                                            <th>Tanggal Surat Selesai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>112233</td>
                                            <td>SPKTP070322001</td>
                                            <td>Agus Susanto</td>
                                            <td>Jl Anggrek</td>
                                            <td>Pengecekan Petugas</td>
                                            <td>Senin, 7 Maret 2022</td>
                                            <td>-</td>
                                        </tr>
                                        <tr>
                                            <td>223344</td>
                                            <td>SPKTP070322002</td>
                                            <td>Budiarta</td>
                                            <td>Jl Garuda</td>
                                            <td>Menunggu Tanda Tangan</td>
                                            <td>Senin, 7 Maret 2022</td>
                                            <td>-</td>
                                        </tr>

                                        <tr>
                                            <td>223355</td>
                                            <td>SPKTP070322002</td>
                                            <td>Agung</td>
                                            <td>Jl Semeru</td>
                                            <td>Selesai</td>
                                            <td>Senin, 7 Maret 2022</td>
                                            <td>Rabu, 9 Maret 2022</td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
</section>