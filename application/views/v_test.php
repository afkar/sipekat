<section class="content">
        <!-- <div class="container-fluid">
            <ol class="breadcrumb breadcrumb-bg-cyan">
                <li><a href="javascript:void(0);"><i class="material-icons">home</i> <?php echo $judul; ?></a></li>
            </ol>
        </div> -->
        <div id="letter-template">
            <p class="letter-first-paragraph">7, Jasmine Road <br> Essex <br> EX36 9EL</p>
            <p class="letter-second-paragraph">Wheeler's Deals <br> 12 Main Street <br> </p>
            <p class="letter-third-paragraph">26th July 2021 </p>
            <p class="letter-name"> Dear Mr Hawkins, </p>
            <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
            <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
            <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ipsum saepe, nihil ipsa ab rem, natus atque odio vero ea voluptatum vitae repellendus eligendi commodi eius officiis eaque molestias sint! Et?</p>
            <p>Your sincerely,</p>
            <!-- <p class="letter-signature"> Mr. Hola </p> -->
            <img style="object-fit:cover;" src="<?=base_url()?>assets/images/signature.png" width="48" height="48" alt="User" />
            <p> Mr. Hola </p>
        </div>
        <a href="javascript:htmlToPdf()">Download File As PDF</a>
</section>