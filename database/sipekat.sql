-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 13 Mar 2022 pada 09.30
-- Versi server: 10.4.22-MariaDB
-- Versi PHP: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sipekat`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_letter`
--

CREATE TABLE `ms_letter` (
  `id` int(11) NOT NULL,
  `letter_name` varchar(200) NOT NULL,
  `description` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_letter_doc`
--

CREATE TABLE `ms_letter_doc` (
  `id` int(11) NOT NULL,
  `letter_id` int(11) NOT NULL,
  `document_requirement` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_role`
--

CREATE TABLE `ms_role` (
  `id` int(11) NOT NULL,
  `role_name` varchar(100) NOT NULL,
  `description` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_status`
--

CREATE TABLE `ms_status` (
  `id` int(11) NOT NULL,
  `request_status` varchar(20) NOT NULL,
  `description` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_user`
--

CREATE TABLE `ms_user` (
  `id` int(11) NOT NULL,
  `NIK` varchar(16) NOT NULL,
  `name` varchar(200) NOT NULL,
  `address` mediumtext NOT NULL,
  `phone_no` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `dob` date NOT NULL,
  `role_id` int(11) NOT NULL,
  `scan_ktp` mediumtext NOT NULL,
  `scan_kk` mediumtext NOT NULL,
  `is_validated` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tr_request`
--

CREATE TABLE `tr_request` (
  `id` int(11) NOT NULL,
  `requester_user_id` int(11) NOT NULL,
  `letter_id` int(11) NOT NULL,
  `request_date` date NOT NULL,
  `file_url` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tr_request_kelahiran`
--

CREATE TABLE `tr_request_kelahiran` (
  `id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `scan_surat_lahir` mediumtext NOT NULL,
  `scan_surat_nikah` mediumtext NOT NULL,
  `scan_kk` mediumtext NOT NULL,
  `scan_ktp_ayah` mediumtext NOT NULL,
  `scan_ktp_ibu` mediumtext NOT NULL,
  `scan_ktp_pelapor` mediumtext NOT NULL,
  `scan_ktp_saksi` mediumtext NOT NULL,
  `scan_sptjm` mediumtext NOT NULL,
  `scan_sktt` mediumtext NOT NULL,
  `scan_paspor` mediumtext NOT NULL,
  `scan_formulir_f101` mediumtext NOT NULL,
  `scan_pengantar` mediumtext NOT NULL,
  `scan_formulir_104` mediumtext NOT NULL,
  `lain_lain_1` mediumtext NOT NULL,
  `lain_lain_2` mediumtext NOT NULL,
  `lain_lain_3` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tr_request_kematian`
--

CREATE TABLE `tr_request_kematian` (
  `id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `scan_ktp_jenazah` mediumtext NOT NULL,
  `scan_kk_jenazah` mediumtext NOT NULL,
  `scan_ktp_saksi_1` mediumtext NOT NULL,
  `scan_ktp_saksi_2` mediumtext NOT NULL,
  `scan_ktp_pelapor` mediumtext NOT NULL,
  `scan_sktt` mediumtext NOT NULL,
  `scan_paspor` mediumtext NOT NULL,
  `lain_lain_1` mediumtext NOT NULL,
  `lain_lain_2` mediumtext NOT NULL,
  `lain_lain_3` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tr_request_ktm`
--

CREATE TABLE `tr_request_ktm` (
  `id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `scan_pengantar` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tr_request_ktp`
--

CREATE TABLE `tr_request_ktp` (
  `id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `scan_kk` mediumtext NOT NULL,
  `scan_ktp_lama` mediumtext NOT NULL,
  `scan_surat_kehilangan` mediumtext NOT NULL,
  `scan_dokumen_pendukung` mediumtext NOT NULL,
  `scan_dokumen_perjalanan` mediumtext NOT NULL,
  `scan_kitap` mediumtext NOT NULL,
  `lain_lain_1` mediumtext NOT NULL,
  `lain_lain_2` mediumtext NOT NULL,
  `lain_lain_3` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tr_request_status`
--

CREATE TABLE `tr_request_status` (
  `id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `employee_user_id` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `description` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `ms_letter`
--
ALTER TABLE `ms_letter`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ms_letter_doc`
--
ALTER TABLE `ms_letter_doc`
  ADD KEY `FK_letter` (`letter_id`);

--
-- Indeks untuk tabel `ms_role`
--
ALTER TABLE `ms_role`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ms_status`
--
ALTER TABLE `ms_status`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ms_user`
--
ALTER TABLE `ms_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_user_role` (`role_id`);

--
-- Indeks untuk tabel `tr_request`
--
ALTER TABLE `tr_request`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_request_letter` (`letter_id`),
  ADD KEY `FK_request_user` (`requester_user_id`);

--
-- Indeks untuk tabel `tr_request_kelahiran`
--
ALTER TABLE `tr_request_kelahiran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_request_kelahiran` (`request_id`);

--
-- Indeks untuk tabel `tr_request_kematian`
--
ALTER TABLE `tr_request_kematian`
  ADD KEY `FK_request_kematian` (`request_id`);

--
-- Indeks untuk tabel `tr_request_ktm`
--
ALTER TABLE `tr_request_ktm`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_request_ktm` (`request_id`);

--
-- Indeks untuk tabel `tr_request_ktp`
--
ALTER TABLE `tr_request_ktp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_request_ktp` (`request_id`);

--
-- Indeks untuk tabel `tr_request_status`
--
ALTER TABLE `tr_request_status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_status_request` (`request_id`),
  ADD KEY `FK_request_employee` (`employee_user_id`),
  ADD KEY `FK_status` (`status_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `ms_letter`
--
ALTER TABLE `ms_letter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ms_role`
--
ALTER TABLE `ms_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ms_status`
--
ALTER TABLE `ms_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ms_user`
--
ALTER TABLE `ms_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tr_request`
--
ALTER TABLE `tr_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tr_request_kelahiran`
--
ALTER TABLE `tr_request_kelahiran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tr_request_ktm`
--
ALTER TABLE `tr_request_ktm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tr_request_ktp`
--
ALTER TABLE `tr_request_ktp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tr_request_status`
--
ALTER TABLE `tr_request_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `ms_letter_doc`
--
ALTER TABLE `ms_letter_doc`
  ADD CONSTRAINT `FK_letter` FOREIGN KEY (`letter_id`) REFERENCES `ms_letter` (`id`);

--
-- Ketidakleluasaan untuk tabel `ms_user`
--
ALTER TABLE `ms_user`
  ADD CONSTRAINT `FK_user_role` FOREIGN KEY (`role_id`) REFERENCES `ms_role` (`id`);

--
-- Ketidakleluasaan untuk tabel `tr_request`
--
ALTER TABLE `tr_request`
  ADD CONSTRAINT `FK_request_letter` FOREIGN KEY (`letter_id`) REFERENCES `ms_letter` (`id`),
  ADD CONSTRAINT `FK_request_user` FOREIGN KEY (`requester_user_id`) REFERENCES `ms_user` (`id`);

--
-- Ketidakleluasaan untuk tabel `tr_request_kelahiran`
--
ALTER TABLE `tr_request_kelahiran`
  ADD CONSTRAINT `FK_request_kelahiran` FOREIGN KEY (`request_id`) REFERENCES `tr_request` (`id`);

--
-- Ketidakleluasaan untuk tabel `tr_request_kematian`
--
ALTER TABLE `tr_request_kematian`
  ADD CONSTRAINT `FK_request_kematian` FOREIGN KEY (`request_id`) REFERENCES `tr_request` (`id`);

--
-- Ketidakleluasaan untuk tabel `tr_request_ktm`
--
ALTER TABLE `tr_request_ktm`
  ADD CONSTRAINT `FK_request_ktm` FOREIGN KEY (`request_id`) REFERENCES `tr_request` (`id`);

--
-- Ketidakleluasaan untuk tabel `tr_request_ktp`
--
ALTER TABLE `tr_request_ktp`
  ADD CONSTRAINT `FK_request_ktp` FOREIGN KEY (`request_id`) REFERENCES `tr_request` (`id`);

--
-- Ketidakleluasaan untuk tabel `tr_request_status`
--
ALTER TABLE `tr_request_status`
  ADD CONSTRAINT `FK_request_employee` FOREIGN KEY (`employee_user_id`) REFERENCES `ms_user` (`id`),
  ADD CONSTRAINT `FK_status` FOREIGN KEY (`status_id`) REFERENCES `ms_status` (`id`),
  ADD CONSTRAINT `FK_status_request` FOREIGN KEY (`request_id`) REFERENCES `tr_request` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
